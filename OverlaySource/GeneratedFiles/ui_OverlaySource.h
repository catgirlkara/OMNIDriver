/********************************************************************************
** Form generated from reading UI file 'OverlaySource.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OVERLAYSOURCE_H
#define UI_OVERLAYSOURCE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OverlaySourceClass
{
public:
    QWidget *centralWidget;
    QTabWidget *tabWidget;
    QWidget *DriverTab;
    QPushButton *installDriverButton;
    QLabel *driverInstallStatusLabel;
    QPushButton *uninstallDriverButton;
    QWidget *OverlayTab;
    QWidget *Omni;
    QSlider *hmdModifier;
    QLabel *label;
    QLabel *label_2;
    QSlider *wand1Modifier;
    QLabel *label_3;
    QSlider *wand2Modifier;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *OverlaySourceClass)
    {
        if (OverlaySourceClass->objectName().isEmpty())
            OverlaySourceClass->setObjectName(QStringLiteral("OverlaySourceClass"));
        OverlaySourceClass->resize(600, 400);
        centralWidget = new QWidget(OverlaySourceClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setGeometry(QRect(-4, -1, 611, 371));
        DriverTab = new QWidget();
        DriverTab->setObjectName(QStringLiteral("DriverTab"));
        installDriverButton = new QPushButton(DriverTab);
        installDriverButton->setObjectName(QStringLiteral("installDriverButton"));
        installDriverButton->setGeometry(QRect(40, 40, 75, 23));
        driverInstallStatusLabel = new QLabel(DriverTab);
        driverInstallStatusLabel->setObjectName(QStringLiteral("driverInstallStatusLabel"));
        driverInstallStatusLabel->setGeometry(QRect(40, 20, 251, 16));
        uninstallDriverButton = new QPushButton(DriverTab);
        uninstallDriverButton->setObjectName(QStringLiteral("uninstallDriverButton"));
        uninstallDriverButton->setGeometry(QRect(200, 40, 75, 23));
        tabWidget->addTab(DriverTab, QString());
        OverlayTab = new QWidget();
        OverlayTab->setObjectName(QStringLiteral("OverlayTab"));
        tabWidget->addTab(OverlayTab, QString());
        Omni = new QWidget();
        Omni->setObjectName(QStringLiteral("Omni"));
        hmdModifier = new QSlider(Omni);
        hmdModifier->setObjectName(QStringLiteral("hmdModifier"));
        hmdModifier->setGeometry(QRect(20, 30, 561, 22));
        hmdModifier->setMaximum(100);
        hmdModifier->setOrientation(Qt::Horizontal);
        label = new QLabel(Omni);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(26, 10, 211, 20));
        label_2 = new QLabel(Omni);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(26, 50, 211, 20));
        wand1Modifier = new QSlider(Omni);
        wand1Modifier->setObjectName(QStringLiteral("wand1Modifier"));
        wand1Modifier->setGeometry(QRect(20, 70, 561, 22));
        wand1Modifier->setMaximum(100);
        wand1Modifier->setOrientation(Qt::Horizontal);
        label_3 = new QLabel(Omni);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(26, 100, 211, 20));
        wand2Modifier = new QSlider(Omni);
        wand2Modifier->setObjectName(QStringLiteral("wand2Modifier"));
        wand2Modifier->setGeometry(QRect(20, 120, 561, 22));
        wand2Modifier->setMaximum(100);
        wand2Modifier->setOrientation(Qt::Horizontal);
        tabWidget->addTab(Omni, QString());
        OverlaySourceClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(OverlaySourceClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 600, 21));
        OverlaySourceClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(OverlaySourceClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        OverlaySourceClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(OverlaySourceClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        OverlaySourceClass->setStatusBar(statusBar);

        retranslateUi(OverlaySourceClass);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(OverlaySourceClass);
    } // setupUi

    void retranslateUi(QMainWindow *OverlaySourceClass)
    {
        OverlaySourceClass->setWindowTitle(QApplication::translate("OverlaySourceClass", "OverlaySource", Q_NULLPTR));
        installDriverButton->setText(QApplication::translate("OverlaySourceClass", "Install", Q_NULLPTR));
        driverInstallStatusLabel->setText(QApplication::translate("OverlaySourceClass", "Driver Installation Status: Installed : Not Installed", Q_NULLPTR));
        uninstallDriverButton->setText(QApplication::translate("OverlaySourceClass", "Uninstall", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(DriverTab), QApplication::translate("OverlaySourceClass", "Driver", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(OverlayTab), QApplication::translate("OverlaySourceClass", "Overlay", Q_NULLPTR));
        label->setText(QApplication::translate("OverlaySourceClass", "HMD Position Offset", Q_NULLPTR));
        label_2->setText(QApplication::translate("OverlaySourceClass", "Vive Wand 1", Q_NULLPTR));
        label_3->setText(QApplication::translate("OverlaySourceClass", "Vive Wand 2", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(Omni), QApplication::translate("OverlaySourceClass", "Omni", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class OverlaySourceClass: public Ui_OverlaySourceClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OVERLAYSOURCE_H

#pragma once

#include <cmath>

inline vr::HmdQuaternion_t operator+(const vr::HmdQuaternion_t& lhs, const vr::HmdQuaternion_t& rhs)
{
	return {
		lhs.w + rhs.w,
		lhs.x + rhs.x,
		lhs.y + rhs.y,
		lhs.z + rhs.z
	};
}


inline vr::HmdQuaternion_t operator-(const vr::HmdQuaternion_t& lhs, const vr::HmdQuaternion_t& rhs)
{
	return{
		lhs.w - rhs.w,
		lhs.x - rhs.x,
		lhs.y - rhs.y,
		lhs.z - rhs.z
	};
}


inline vr::HmdQuaternion_t operator*(const vr::HmdQuaternion_t& lhs, const vr::HmdQuaternion_t& rhs)
{
	return {
		(lhs.w * rhs.w) - (lhs.x * rhs.x) - (lhs.y * rhs.y) - (lhs.z * rhs.z),
		(lhs.w * rhs.x) + (lhs.x * rhs.w) + (lhs.y * rhs.z) - (lhs.z * rhs.y),
		(lhs.w * rhs.y) + (lhs.y * rhs.w) + (lhs.z * rhs.x) - (lhs.x * rhs.z),
		(lhs.w * rhs.z) + (lhs.z * rhs.w) + (lhs.x * rhs.y) - (lhs.y * rhs.x)
	};
}


inline vr::HmdVector3d_t operator+(const vr::HmdVector3d_t& lhs, const vr::HmdVector3d_t& rhs)
{
	return {
		lhs.v[0] + rhs.v[0],
		lhs.v[1] + rhs.v[1],
		lhs.v[2] + rhs.v[2]
	};
}
inline vr::HmdVector3d_t operator+=(vr::HmdVector3d_t& lhs, const vr::HmdVector3d_t& rhs)
{
	lhs = lhs + rhs;
	return lhs;
}

inline vr::HmdVector3d_t operator+(const vr::HmdVector3d_t& lhs, const double(&rhs)[3])
{
	return{
		lhs.v[0] + rhs[0],
		lhs.v[1] + rhs[1],
		lhs.v[2] + rhs[2]
	};
}

inline vr::HmdVector3d_t operator-(const vr::HmdVector3d_t& lhs, const vr::HmdVector3d_t& rhs)
{
	return{
		lhs.v[0] - rhs.v[0],
		lhs.v[1] - rhs.v[1],
		lhs.v[2] - rhs.v[2]
	};
}

inline vr::HmdVector3d_t operator-(const vr::HmdVector3d_t& lhs, const double(&rhs)[3])
{
	return{
		lhs.v[0] - rhs[0],
		lhs.v[1] - rhs[1],
		lhs.v[2] - rhs[2]
	};
}


inline vr::HmdVector3d_t operator*(const vr::HmdVector3d_t& lhs, const double rhs)
{
	return{
		lhs.v[0] * rhs,
		lhs.v[1] * rhs,
		lhs.v[2] * rhs
	};
}


inline vr::HmdVector3d_t operator/(const vr::HmdVector3d_t& lhs, const double rhs)
{
	return{
		lhs.v[0] / rhs,
		lhs.v[1] / rhs,
		lhs.v[2] / rhs
	};
}


namespace vrmath
{
	const float PI = 3.141592653589793f;

	inline double sqrMag(const vr::HmdVector3d_t vec)
	{
		return vec.v[0] * vec.v[0] + vec.v[1] * vec.v[1] + vec.v[2] * vec.v[2];
	}

	inline float deg2rad(float degrees)
	{
		return degrees * PI / 180.0f;
	}

	inline float rad2deg(float angle)
	{
		return angle * 180.0f / PI;
	}

	inline vr::HmdQuaternion_t quaternionInverse(const vr::HmdQuaternion_t& rot)
	{
		double d = rot.w * rot.w + rot.x * rot.x + rot.y * rot.y + rot.z * rot.z;
		return {rot.w / d, -rot.x / d, -rot.y / d, -rot.z / d};
	}

	inline vr::HmdQuaternion_t quaternionFromRotationAxis(double rot, double ux, double uy, double uz)
	{
		auto ha = rot / 2;
		return{
			std::cos(ha),
			ux * std::sin(ha),
			uy * std::sin(ha),
			uz * std::sin(ha)
		};
	}

	inline vr::HmdQuaternion_t quaternionFromRotationX(double rot)
	{
		auto ha = rot / 2;
		return{
			std::cos(ha),
			std::sin(ha),
			0.0f,
			0.0f
		};
	}

	inline vr::HmdQuaternion_t quaternionFromRotationY(double rot)
	{
		auto ha = rot / 2;
		return{
			std::cos(ha),
			0.0f,
			std::sin(ha),
			0.0f
		};
	}

	inline vr::HmdQuaternion_t quaternionFromRotationZ(double rot)
	{
		auto ha = rot / 2;
		return{
			std::cos(ha),
			0.0f,
			0.0f,
			std::sin(ha)
		};
	}

	inline vr::HmdQuaternion_t normalized(const vr::HmdQuaternion_t& quat)
	{
		vr::HmdQuaternion_t q;
		double mag = sqrt(quat.w * quat.w + quat.x * quat.x + quat.y * quat.y + quat.z * quat.z);
		if(mag != 0)
		{
			q.w /= mag;
			q.x /= mag;
			q.y /= mag;
			q.z /= mag;
		}
		return q;
	}

	inline vr::HmdQuaternion_t quaternionFromYaw(const vr::HmdQuaternion_t& quat)
	{
		vr::HmdQuaternion_t q;
		q.w = quat.w;
		q.x = 0;
		q.y = quat.y;
		q.z = 0;
		return normalized(q);
	}
	inline vr::HmdVector3_t EulerFromQuaternion(const vr::HmdQuaternion_t& q)
	{
		float yaw, pitch, roll;

		double sqw = q.w * q.w;
		double sqx = q.x * q.x;
		double sqy = q.y * q.y;
		double sqz = q.z * q.z;

		// If quaternion is normalised the unit is one, otherwise it is the correction factor
		double unit = sqx + sqy + sqz + sqw;
		double test = q.x * q.y + q.z * q.w;

		if(test > 0.4999f * unit)                              // 0.4999f or 0.5f - epsilon
		{
			// singularity at north pole
			yaw = 2.0f * (float)atan2(q.x, q.w);  // yaw
			pitch = PI * 0.5f;                         // pitch
			roll = 0.0f;                                // roll
		}
		else if(test < -0.4999f * unit)                        // -0.4999f or -0.5f + epsilon
		{
			// singularity at south pole
			yaw = -2.0f * (float)atan2(q.x, q.w); // yaw
			pitch = -PI * 0.5f;                        // pitch
			roll = 0.0f;                                // Roll
		}
		else
		{
			yaw = (float)atan2(2.0f * q.x * q.w + 2.0f * q.y * q.z, 1 - 2.0f * (sqz + sqw));     // yaw 
			pitch = (float)asin(2.0f * (q.x * q.z - q.w * q.y));                             // pitch 
			roll = (float)atan2(2.0f * q.x * q.y + 2.0f * q.z * q.w, 1 - 2.0f * (sqy + sqz));      // Roll 
		}

		return {pitch, yaw, roll};

		// roll (x-axis rotation)
		double sinr = +2.0 * (q.w * q.x + q.y * q.z);
		double cosr = +1.0 - 2.0 * (q.x * q.x + q.y * q.y);
		roll = atan2(sinr, cosr);

		// pitch (y-axis rotation)
		double sinp = +2.0 * (q.w * q.y - q.z * q.x);
		if(fabs(sinp) >= 1)
			pitch = copysign(PI / 2, sinp); // use 90 degrees if out of range
		else
			pitch = asin(sinp);

		// yaw (z-axis rotation)
		double siny = +2.0 * (q.w * q.z + q.x * q.y);
		double cosy = +1.0 - 2.0 * (q.y * q.y + q.z * q.z);
		yaw = atan2(siny, cosy);

		return {pitch, yaw, roll};
	}
	inline vr::HmdQuaternion_t QuaternionFromEuler(double pitch, double yaw, double roll)
	{
		vr::HmdQuaternion_t q;

		// Abbreviations for the various angular functions
		double cy = cos(roll * 0.5);
		double sy = sin(roll * 0.5);
		double cr = cos(pitch * 0.5);
		double sr = sin(pitch * 0.5);
		double cp = cos(yaw * 0.5);
		double sp = sin(yaw * 0.5);

		q.w = cy * cr * cp + sy * sr * sp;
		q.x = cy * sr * cp - sy * cr * sp;
		q.y = cy * cr * sp + sy * sr * cp;
		q.z = sy * cr * cp - cy * sr * sp;
		return q;
	}

	inline vr::HmdVector3_t DegreesFromQuaternion(const vr::HmdQuaternion_t& q)
	{
		auto vec = EulerFromQuaternion(q);
		return {rad2deg(vec.pitch), rad2deg(vec.yaw), rad2deg(vec.roll)};
	}
	inline vr::HmdQuaternion_t QuaternionFromDegrees(double pitch, double yaw, double roll)
	{
		return QuaternionFromEuler(deg2rad(pitch), deg2rad(yaw), deg2rad(roll));
	}

	inline vr::HmdQuaternion_t quaternionFromYawPitchRoll(double yaw, double pitch, double roll)
	{
		return quaternionFromRotationY(yaw) * quaternionFromRotationX(pitch) * quaternionFromRotationZ(roll);
	}

	inline vr::HmdQuaternion_t quaternionFromRotationMatrix(const vr::HmdMatrix34_t& mat)
	{
		auto a = mat.m;
		vr::HmdQuaternion_t q;
		double trace = a[0][0] + a[1][1] + a[2][2];
		if(trace > 0)
		{
			double s = 0.5 / sqrt(trace + 1.0);
			q.w = 0.25 / s;
			q.x = (a[1][2] - a[2][1]) * s;
			q.y = (a[2][0] - a[0][2]) * s;
			q.z = (a[0][1] - a[1][0]) * s;
		}
		else
		{
			if(a[0][0] > a[1][1] && a[0][0] > a[2][2])
			{
				double s = 2.0 * sqrt(1.0 + a[0][0] - a[1][1] - a[2][2]);
				q.w = (a[1][2] - a[2][1]) / s;
				q.x = 0.25 * s;
				q.y = (a[1][0] + a[0][1]) / s;
				q.z = (a[2][0] + a[0][2]) / s;
			}
			else if(a[1][1] > a[2][2])
			{
				double s = 2.0 * sqrt(1.0 + a[1][1] - a[0][0] - a[2][2]);
				q.w = (a[2][0] - a[0][2]) / s;
				q.x = (a[1][0] + a[0][1]) / s;
				q.y = 0.25 * s;
				q.z = (a[2][1] + a[1][2]) / s;
			}
			else
			{
				double s = 2.0 * sqrt(1.0 + a[2][2] - a[0][0] - a[1][1]);
				q.w = (a[0][1] - a[1][0]) / s;
				q.x = (a[2][0] + a[0][2]) / s;
				q.y = (a[2][1] + a[1][2]) / s;
				q.z = 0.25 * s;
			}
		}
		q.x = -q.x;
		q.y = -q.y;
		q.z = -q.z;
		return q;
	}

	inline vr::HmdQuaternion_t quaternionConjugate(const vr::HmdQuaternion_t& quat)
	{
		return {
			quat.w,
			-quat.x,
			-quat.y,
			-quat.z,
		};
	}

	inline vr::HmdVector3d_t QuaternionRotateVector(const vr::HmdQuaternion_t& quat, const vr::HmdVector3d_t& vector, bool reverse = false)
	{
		if(reverse)
		{
			vr::HmdQuaternion_t pin = {0.0, vector.v[0], vector.v[1] , vector.v[2]};
			auto pout = vrmath::quaternionConjugate(quat) * pin * quat;
			return {pout.x, pout.y, pout.z};
		}
		else
		{
			vr::HmdQuaternion_t pin = {0.0, vector.v[0], vector.v[1] , vector.v[2]};
			auto pout = quat * pin * vrmath::quaternionConjugate(quat);
			return {pout.x, pout.y, pout.z};
		}
	}

	inline vr::HmdVector3d_t QuaternionRotateVector(const vr::HmdQuaternion_t& quat, const vr::HmdQuaternion_t& quatInv, const vr::HmdVector3d_t& vector, bool reverse = false)
	{
		if(reverse)
		{
			vr::HmdQuaternion_t pin = {0.0, vector.v[0], vector.v[1] , vector.v[2]};
			auto pout = quatInv * pin * quat;
			return{pout.x, pout.y, pout.z};
		}
		else
		{
			vr::HmdQuaternion_t pin = {0.0, vector.v[0], vector.v[1] , vector.v[2]};
			auto pout = quat * pin * quatInv;
			return{pout.x, pout.y, pout.z};
		}
	}

	inline vr::HmdVector3d_t QuaternionRotateVector(const vr::HmdQuaternion_t& quat, const double(&vector)[3], bool reverse = false)
	{
		if(reverse)
		{
			vr::HmdQuaternion_t pin = {0.0, vector[0], vector[1] , vector[2]};
			auto pout = vrmath::quaternionConjugate(quat) * pin * quat;
			return{pout.x, pout.y, pout.z};
		}
		else
		{
			vr::HmdQuaternion_t pin = {0.0, vector[0], vector[1] , vector[2]};
			auto pout = quat * pin * vrmath::quaternionConjugate(quat);
			return{pout.x, pout.y, pout.z};
		}
	}

	inline vr::HmdVector3d_t QuaternionRotateVector(const vr::HmdQuaternion_t& quat, const vr::HmdQuaternion_t& quatInv, const double(&vector)[3], bool reverse = false)
	{
		if(reverse)
		{
			vr::HmdQuaternion_t pin = {0.0, vector[0], vector[1] , vector[2]};
			auto pout = quatInv * pin * quat;
			return{pout.x, pout.y, pout.z};
		}
		else
		{
			vr::HmdQuaternion_t pin = {0.0, vector[0], vector[1] , vector[2]};
			auto pout = quat * pin * quatInv;
			return{pout.x, pout.y, pout.z};
		}
	}

	inline vr::HmdMatrix34_t matMul33(const vr::HmdMatrix34_t& a, const vr::HmdMatrix34_t& b)
	{
		vr::HmdMatrix34_t result;
		for(unsigned i = 0; i < 3; i++)
		{
			for(unsigned j = 0; j < 3; j++)
			{
				result.m[i][j] = 0.0f;
				for(unsigned k = 0; k < 3; k++)
				{
					result.m[i][j] += a.m[i][k] * b.m[k][j];
				}
			}
		}
		return result;
	}

	inline vr::HmdVector3_t matMul33(const vr::HmdMatrix34_t& a, const vr::HmdVector3_t& b)
	{
		vr::HmdVector3_t result;
		for(unsigned i = 0; i < 3; i++)
		{
			result.v[i] = 0.0f;
			for(unsigned k = 0; k < 3; k++)
			{
				result.v[i] += a.m[i][k] * b.v[k];
			};
		}
		return result;
	}

	inline vr::HmdVector3d_t matMul33(const vr::HmdMatrix34_t& a, const vr::HmdVector3d_t& b)
	{
		vr::HmdVector3d_t result;
		for(unsigned i = 0; i < 3; i++)
		{
			result.v[i] = 0.0f;
			for(unsigned k = 0; k < 3; k++)
			{
				result.v[i] += a.m[i][k] * b.v[k];
			};
		}
		return result;
	}

	inline vr::HmdVector3_t matMul33(const vr::HmdVector3_t& a, const vr::HmdMatrix34_t& b)
	{
		vr::HmdVector3_t result;
		for(unsigned i = 0; i < 3; i++)
		{
			result.v[i] = 0.0f;
			for(unsigned k = 0; k < 3; k++)
			{
				result.v[i] += a.v[k] * b.m[k][i];
			};
		}
		return result;
	}

	inline vr::HmdVector3d_t matMul33(const vr::HmdVector3d_t& a, const vr::HmdMatrix34_t& b)
	{
		vr::HmdVector3d_t result;
		for(unsigned i = 0; i < 3; i++)
		{
			result.v[i] = 0.0f;
			for(unsigned k = 0; k < 3; k++)
			{
				result.v[i] += a.v[k] * b.m[k][i];
			};
		}
		return result;
	}

	inline vr::HmdMatrix34_t transposeMul33(const vr::HmdMatrix34_t& a)
	{
		vr::HmdMatrix34_t result;
		for(unsigned i = 0; i < 3; i++)
		{
			for(unsigned k = 0; k < 3; k++)
			{
				result.m[i][k] = a.m[k][i];
			}
		}
		result.m[0][3] = a.m[0][3];
		result.m[1][3] = a.m[1][3];
		result.m[2][3] = a.m[2][3];
		return result;
	}
}


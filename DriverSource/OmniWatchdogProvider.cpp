#include "OmniDriver.h"
#include "OmniLog.h"

vr::EVRInitError OmniWatchdogProvider::Init(vr::IVRDriverContext* context)
{
	//vr::EVRInitError initError = vr::InitWatchdogDriverContext(context);
	//if(initError != vr::VRInitError_None)
	//{
	//	return initError;
	//}
	OmniLog::AppendLine("Watchdog Initialized");

	VR_INIT_WATCHDOG_DRIVER_CONTEXT(context);
	return vr::EVRInitError::VRInitError_None;
}

void OmniWatchdogProvider::Cleanup()
{

	VR_CLEANUP_WATCHDOG_DRIVER_CONTEXT();
}


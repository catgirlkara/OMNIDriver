#include "OmniDriver.h"
#include "OmniLog.h"

EVRInitError OmniDevice::Activate(uint32_t unObjectId)
{
	return EVRInitError::VRInitError_None;
}

void OmniDevice::Deactivate()
{
}

void OmniDevice::EnterStandby()
{
}

void * OmniDevice::GetComponent(const char * pchComponentNameAndVersion)
{
	OmniLog::AppendLine("GetComponent(%s)", pchComponentNameAndVersion);
	return nullptr;
}

void OmniDevice::DebugRequest(const char * pchRequest, char * pchResponseBuffer, uint32_t unResponseBufferSize)
{
}

DriverPose_t OmniDevice::GetPose()
{
	return DriverPose_t();
}

#include <Windows.h>
#include "OmniDriver.h"
#include "OmniLog.h"
#include <stack>
#include "vtablehook.h"
#include "openvr_math.h"
#include <algorithm>


static const char *IVRControllerComponent_Version = "IVRControllerComponent_001";


typedef void(*TrackedDeviceAddedFunc)(vr::IVRServerDriverHost*, const char *, ETrackedDeviceClass, ITrackedDeviceServerDriver *);
TrackedDeviceAddedFunc TrackedDeviceAddedOrig;

typedef void(*TrackedDevicePoseUpdatedFunc)(vr::IVRServerDriverHost*, uint32_t, const vr::DriverPose_t&, uint32_t);
TrackedDevicePoseUpdatedFunc TrackedDevicePoseUpdatedOrig;

typedef bool(*PollNextEventFunc)(vr::IVRServerDriverHost*, VREvent_t*, uint32_t);
PollNextEventFunc PollNextEventOrig;

typedef EVRInputError(*CreateBooleanComponentFunc)(vr::IVRDriverInput*, PropertyContainerHandle_t, const char *, VRInputComponentHandle_t *);
CreateBooleanComponentFunc CreateBooleanComponentOrig;

typedef EVRInputError(*CreateScalarComponentFunc)(vr::IVRDriverInput*, PropertyContainerHandle_t, const char *, VRInputComponentHandle_t *, EVRScalarType, EVRScalarUnits);
CreateScalarComponentFunc CreateScalarComponentOrig;

typedef EVRInputError(*UpdateBooleanComponentFunc)(vr::IVRDriverInput*, PropertyContainerHandle_t, bool, double);
UpdateBooleanComponentFunc UpdateBooleanComponentOrig;

typedef EVRInputError(*UpdateScalarComponentFunc)(vr::IVRDriverInput*, VRInputComponentHandle_t, float, double);
UpdateScalarComponentFunc UpdateScalarComponentOrig;


EVRInitError DeviceActivate(vr::ITrackedDeviceServerDriver* pDriver, uint32_t deviceIndex);
void* DeviceGetComponent(vr::ITrackedDeviceServerDriver* pDriver, const char* interfaceVersion);

typedef EVRInitError(*DeviceActivateFunc)(vr::ITrackedDeviceServerDriver*, uint32_t);
typedef EVRInitError(*DeviceActivateFunc)(vr::ITrackedDeviceServerDriver*, uint32_t);
typedef void*(*DeviceGetComponentFunc)(vr::ITrackedDeviceServerDriver*, const char *);
class TrackedDeviceHooks
{
	public:
	TrackedDeviceHooks()
	{
	}
	TrackedDeviceHooks(vr::ITrackedDeviceServerDriver* pDriver)
	{
		OmniLog::AppendLine("Hooking Tracked Device %p", pDriver);
		ActivateOrig = (DeviceActivateFunc)vtablehook_hook(pDriver, DeviceActivate, 0);
		//GetComponentOrig = (DeviceGetComponentFunc)vtablehook_hook(pDriver, DeviceGetComponent, 3);
	}
	DeviceActivateFunc ActivateOrig;
	DeviceGetComponentFunc GetComponentOrig;
};
std::map<intptr_t, TrackedDeviceHooks> TrackedDeviceHookMap;

class DeviceInfo
{
	public:
	vr::ITrackedDeviceServerDriver* driver;
	uint32_t index;
	PropertyContainerHandle_t propertiesHandle;
	float omniModifier = 0.0f;
	//std::map<VRInputComponentHandle_t, std::string> inputs;
	VRInputComponentHandle_t touch = k_ulInvalidInputComponentHandle;
	VRInputComponentHandle_t x = k_ulInvalidInputComponentHandle;
	VRInputComponentHandle_t y = k_ulInvalidInputComponentHandle;
};
DeviceInfo hmdDevice;
DeviceInfo wand1Device;
DeviceInfo wand2Device;


vr::HmdVector3d_t prevAxisValue;
vr::HmdVector3d_t offset;
HmdVector3d_t omniAxis;
float omniYaw;

EVRInitError DeviceActivate(vr::ITrackedDeviceServerDriver* pDriver, uint32_t deviceIndex)
{
	OmniLog::AppendLine("Activating Tracked Device %p", pDriver);
	auto vtablePointer = GetVTablePointer(pDriver);
	if(pDriver == hmdDevice.driver)
	{
		hmdDevice.index = deviceIndex;
		hmdDevice.propertiesHandle = vr::VRProperties()->TrackedDeviceToPropertyContainer(deviceIndex);
		OmniLog::AppendLine("Hmd index(%s) propertyHandle(%s)", std::to_string(deviceIndex), std::to_string(hmdDevice.propertiesHandle));
	}
	if(pDriver == wand1Device.driver)
	{
		wand1Device.index = deviceIndex;
		wand1Device.propertiesHandle = vr::VRProperties()->TrackedDeviceToPropertyContainer(deviceIndex);
		OmniLog::AppendLine("wand1 index(%s) propertyHandle(%s)", std::to_string(deviceIndex), std::to_string(wand1Device.propertiesHandle));
	}
	if(pDriver == wand2Device.driver)
	{
		wand2Device.index = deviceIndex;
		wand2Device.propertiesHandle = vr::VRProperties()->TrackedDeviceToPropertyContainer(deviceIndex);
		OmniLog::AppendLine("wand2 index(%s) propertyHandle(%s)", std::to_string(deviceIndex), std::to_string(wand2Device.propertiesHandle));
	}

	return TrackedDeviceHookMap[vtablePointer].ActivateOrig(pDriver, deviceIndex);
}

void* DeviceGetComponent(vr::ITrackedDeviceServerDriver* pDriver, const char* interfaceVersion)
{
	auto vtablePointer = GetVTablePointer(pDriver);

	void* component = TrackedDeviceHookMap[vtablePointer].GetComponentOrig(pDriver, interfaceVersion);
	OmniLog::AppendLine("%p GetComponent(\"%s\") is %p", pDriver, interfaceVersion, component);
	return component;
}

void TrackedDeviceAdded(vr::IVRServerDriverHost* server, const char *pchDeviceSerialNumber, ETrackedDeviceClass eDeviceClass, ITrackedDeviceServerDriver *pDriver)
{
	OmniLog::AppendLine("Adding Tracked Device %p", pDriver);
	if(eDeviceClass == ETrackedDeviceClass::TrackedDeviceClass_HMD)
	{
		hmdDevice.driver = pDriver;
	}
	else if(eDeviceClass == ETrackedDeviceClass::TrackedDeviceClass_Controller)
	{
		if(wand1Device.driver == nullptr)
		{
			wand1Device.driver = pDriver;
		}
		else if(wand2Device.driver == nullptr)
		{
			wand2Device.driver = pDriver;
		}
	}
	auto vtablePointer = GetVTablePointer(pDriver);
	if(TrackedDeviceHookMap.find(vtablePointer) == TrackedDeviceHookMap.end())
	{
		OmniLog::AppendLine("Inserting new vTable entry for device %p", pDriver);
		TrackedDeviceHookMap.insert({vtablePointer, TrackedDeviceHooks(pDriver)});
	}

	TrackedDeviceAddedOrig(server, pchDeviceSerialNumber, eDeviceClass, pDriver);
}

void TrackedDevicePoseUpdated(vr::IVRServerDriverHost* server, uint32_t deviceIndex, const vr::DriverPose_t & constPose, uint32_t poseSize)
{
	float speedModifier = hmdDevice.omniModifier * 0.05f;
	vr::DriverPose_t pose = constPose;
	pose.vecWorldFromDriverTranslation[0] += offset.v[0] * speedModifier;
	pose.vecWorldFromDriverTranslation[1] += offset.v[1] * speedModifier;
	pose.vecWorldFromDriverTranslation[2] += offset.v[2] * speedModifier;
	pose.vecVelocity[0] += prevAxisValue.v[0] * speedModifier;
	pose.vecVelocity[1] += prevAxisValue.v[1] * speedModifier;
	pose.vecVelocity[2] += prevAxisValue.v[2] * speedModifier;

	if(deviceIndex == wand1Device.index)
	{
		OmniLog::AppendLine("wand1 Yaw(%f) omniYaw(%f)", vrmath::EulerFromQuaternion(pose.qRotation).yaw, omniYaw);

		auto wandEuler = vrmath::EulerFromQuaternion(pose.qRotation);
		wandEuler.yaw = omniYaw;
		//pose.qRotation = vrmath::QuaternionFromEuler(wandEuler.pitch, wandEuler.yaw, wandEuler.roll);
		//pose.qRotation = vrmath::QuaternionFromEuler(0, omniYaw, 0);
	}

	TrackedDevicePoseUpdatedOrig(server, deviceIndex, pose, poseSize);

}

bool PollNextEvent(vr::IVRServerDriverHost* server, VREvent_t* pEvent, uint32_t uncbVREvent)
{
	if(pEvent->eventType > 199 && pEvent->eventType < 300)
	{
		OmniLog::AppendLine("Poll Event(%i)", (EVREventType)pEvent->eventType);
		VREvent_Controller_t data = pEvent->data.controller;
	}
	return PollNextEventOrig(server, pEvent, uncbVREvent);
}

EVRInputError CreateBooleanComponent(IVRDriverInput* input, PropertyContainerHandle_t ulContainer, const char *pchName, VRInputComponentHandle_t *pHandle)
{
	auto retVal = CreateBooleanComponentOrig(input, ulContainer, pchName, pHandle);
	OmniLog::AppendLine("CreateBool, ContainerHandle(%s), Name(%s), ComponentHandle(%s)", std::to_string(ulContainer), pchName, std::to_string(*pHandle));
	if(*pHandle != k_ulInvalidInputComponentHandle)
	{
		if(ulContainer == wand1Device.propertiesHandle)
		{
			if(std::string(pchName) == "/input/trackpad/touch")
			{
				OmniLog::AppendLine("Setting wand1 Touch {%s, %s}", std::to_string(*pHandle), pchName);
				wand1Device.touch = *pHandle;
			}
			//OmniLog::AppendLine("Adding Input to Wand1 {%s, %s}", std::to_string(*pHandle), pchName);
			//wand1Controller.insert({*pHandle, std::string(pchName)});
		}
		else if(ulContainer == wand2Device.propertiesHandle)
		{
			//OmniLog::AppendLine("Adding Input to Wand2 {%s, %s}", std::to_string(*pHandle), pchName);
			//wand2Controller.insert({*pHandle, std::string(pchName)});
		}
	}
	return retVal;
}
EVRInputError CreateScalarComponent(IVRDriverInput* input, PropertyContainerHandle_t ulContainer, const char *pchName, VRInputComponentHandle_t *pHandle, EVRScalarType eType, EVRScalarUnits eUnits)
{
	auto retVal = CreateScalarComponentOrig(input, ulContainer, pchName, pHandle, eType, eUnits);
	OmniLog::AppendLine("CreateScalar, ContainerHandle(%s), Name(%s), ComponentHandle(%s), ScalarType(%i), ScalarUnits(%i)", std::to_string(ulContainer), pchName, std::to_string(*pHandle), eType, eUnits);
	if(*pHandle != k_ulInvalidInputComponentHandle)
	{
		if(ulContainer == wand1Device.propertiesHandle)
		{
			if(std::string(pchName) == "/input/trackpad/x")
			{
				OmniLog::AppendLine("Setting wand1 x {%s, %s}", std::to_string(*pHandle), pchName);
				wand1Device.x = *pHandle;
			}
			else if(std::string(pchName) == "/input/trackpad/y")
			{
				OmniLog::AppendLine("Setting wand1 y {%s, %s}", std::to_string(*pHandle), pchName);
				wand1Device.y = *pHandle;
			}
		}
		else if(ulContainer == wand2Device.propertiesHandle)
		{
			//OmniLog::AppendLine("Adding Input to Wand2 {%s, %s}", std::to_string(*pHandle), pchName);
			//wand2Controller.insert({*pHandle, std::string(pchName)});
		}
	}
	return retVal;
}

EVRInputError UpdateBooleanComponent(IVRDriverInput* input, VRInputComponentHandle_t ulComponent, bool bNewValue, double fTimeOffset)
{
	OmniLog::AppendLine("UpdateBool, ComponentHandle(%s)", std::to_string(ulComponent));

	if(ulComponent == wand1Device.touch)
	{
		OmniLog::AppendLine("Overwriting trackpad for wand1");
		return EVRInputError::VRInputError_None;
	}

	return UpdateBooleanComponentOrig(input, ulComponent, bNewValue, fTimeOffset);
}
EVRInputError UpdateScalarComponent(IVRDriverInput* input, VRInputComponentHandle_t ulComponent, float fNewValue, double fTimeOffset)
{
	OmniLog::AppendLine("UpdateScalar, ComponentHandle(%s)", std::to_string(ulComponent));

	if(ulComponent == wand1Device.x || ulComponent == wand1Device.y)
	{
		OmniLog::AppendLine("Overwriting trackpad for wand1");
		return EVRInputError::VRInputError_None;
	}

	return UpdateScalarComponentOrig(input, ulComponent, fNewValue, fTimeOffset);
}

vr::EVRInitError OmniServerProvider::Init(vr::IVRDriverContext* context)
{
	vr::EVRInitError initError = vr::InitServerDriverContext(context);
	if(initError != vr::VRInitError_None)
	{
		return initError;
	}
	OmniLog::AppendLine("OMNI Server Init");

	auto serverHost = vr::VRServerDriverHost();
	TrackedDeviceAddedOrig = (TrackedDeviceAddedFunc)vtablehook_hook(serverHost, TrackedDeviceAdded, 0);
	TrackedDevicePoseUpdatedOrig = (TrackedDevicePoseUpdatedFunc)vtablehook_hook(serverHost, TrackedDevicePoseUpdated, 1);
	PollNextEventOrig = (PollNextEventFunc)vtablehook_hook(serverHost, PollNextEvent, 5);

	auto input = vr::VRDriverInput();
	CreateBooleanComponentOrig = (CreateBooleanComponentFunc)vtablehook_hook(input, CreateBooleanComponent, 0);
	UpdateBooleanComponentOrig = (UpdateBooleanComponentFunc)vtablehook_hook(input, UpdateBooleanComponent, 1);
	CreateScalarComponentOrig = (CreateScalarComponentFunc)vtablehook_hook(input, CreateScalarComponent, 2);
	UpdateScalarComponentOrig = (UpdateScalarComponentFunc)vtablehook_hook(input, UpdateScalarComponent, 3);

	wand1Device.omniModifier = 3;

	return vr::EVRInitError::VRInitError_None;
}

void OmniServerProvider::Cleanup()
{
	vtablehook_hook(vr::VRServerDriverHost(), TrackedDeviceAddedOrig, 0);
	vtablehook_hook(vr::VRServerDriverHost(), TrackedDevicePoseUpdatedOrig, 1);
}

const char* const* OmniServerProvider::GetInterfaceVersions()
{
	return vr::k_InterfaceVersions;
}

void OmniServerProvider::RunFrame()
{
	auto& input = InputManager::GetInstance();

	if(input.GetOmniInput(omniAxis, omniYaw))
	{
		//if(true)
		if(vrmath::sqrMag(omniAxis) > 0.0f)
		{
			OmniLog::AppendLine("SqrMag(%s)", std::to_string(vrmath::sqrMag(omniAxis)));
			if(hmdDevice.driver != nullptr)
			{
				OmniLog::AppendLine("HMD Yaw(%f)", vrmath::EulerFromQuaternion(hmdDevice.driver->GetPose().qRotation).v[1]);
				//omniAxis = vrmath::quaternionRotateVector(omniRotation, omniAxis);
				offset += omniAxis;
				prevAxisValue = omniAxis;
			}
			if(wand1Device.driver != nullptr)
			{
				auto wand1Pose = wand1Device.driver->GetPose();
				auto wand1Yaw = vrmath::quaternionInverse(vrmath::quaternionFromYaw(wand1Device.driver->GetPose().qRotation));
				//auto wand1Axis = vrmath::quaternionRotateVector(wand1Yaw, omniAxis) * wand1Device.omniModifier;
				auto wand1Axis = omniAxis * wand1Device.omniModifier;
				if(wand1Device.touch != k_ulInvalidInputComponentHandle)
				{
					UpdateBooleanComponentOrig(vr::VRDriverInput(), wand1Device.touch, true, 0);
				}

				if(wand1Device.x != k_ulInvalidInputComponentHandle)
				{
					UpdateScalarComponentOrig(vr::VRDriverInput(), wand1Device.x, wand1Axis.v[0], 0);
				}

				if(wand1Device.y != k_ulInvalidInputComponentHandle)
				{
					UpdateScalarComponentOrig(vr::VRDriverInput(), wand1Device.y, wand1Axis.v[2], 0);
				}
			}
		}
		else
		{
			if(wand1Device.touch != k_ulInvalidInputComponentHandle)
			{
				UpdateBooleanComponentOrig(vr::VRDriverInput(), wand1Device.touch, false, 0);
			}

			if(wand1Device.x != k_ulInvalidInputComponentHandle)
			{
				UpdateScalarComponentOrig(vr::VRDriverInput(), wand1Device.x, 0, 0);
			}

			if(wand1Device.y != k_ulInvalidInputComponentHandle)
			{
				UpdateScalarComponentOrig(vr::VRDriverInput(), wand1Device.y, 0, 0);
			}
		}
	}


	//if(wand1Controller == nullptr)
	//{
	//	wand1Controller = wand1Device->GetComponent("/input/trackpad/touch");
	//	if(wand1Controller != nullptr)
	//	{
	//		OmniLog::AppendLine("Wand 1 Controller Component found");
	//	}
	//}
	//if(wand2Controller == nullptr)
	//{
	//	wand2Controller = wand2Device->GetComponent(vr::IVRDriverInput_Version);
	//	if(wand2Controller != nullptr)
	//	{
	//		OmniLog::AppendLine("Wand 2 Controller Component found");
	//	}
	//}
}

bool OmniServerProvider::ShouldBlockStandbyMode()
{
	return false;
}

void OmniServerProvider::EnterStandby()
{
}

void OmniServerProvider::LeaveStandby()
{
}

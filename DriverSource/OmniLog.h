#include <string>

#pragma once
class OmniLog
{
	public:
	static std::string Format(const char* format, va_list args);
	static std::string Format(const char* format, ...);
	static void Append(const char* format, ...);
	static void AppendLine(const char* format, ...);
	private:
	OmniLog();
	~OmniLog();
};


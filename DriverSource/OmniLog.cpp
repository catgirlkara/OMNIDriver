#include "openvr_driver.h"
#include <Windows.h>
#include "OmniLog.h"


std::string OmniLog::Format(const char* format, va_list args)
{
	int size = _vscprintf(format, args);
	std::string result(++size, 0);
	vsnprintf_s((char*)result.data(), size, _TRUNCATE, format, args);
	return result;
}

std::string OmniLog::Format(const char* format, ...)
{
	va_list args;
	va_start(args, format);
	auto result = Format(format, args);
	va_end(args);
	return result;
}

void OmniLog::Append(const char* format, ...)
{
	vr::IVRDriverLog* log = vr::VRDriverLog();
	va_list args;
	va_start(args, format);
	auto result = Format(format, args);
	va_end(args);
	if(log)
	{
		log->Log(result.c_str());
	}
}

void OmniLog::AppendLine(const char* format, ...)
{
	vr::IVRDriverLog* log = vr::VRDriverLog();
	va_list args;
	va_start(args, format);
	auto result = Format(format, args);
	va_end(args);
	result = Format("\n\tOMNI: %s\n", result.c_str());
	if(log)
	{
		log->Log(result.c_str());
	}
}

OmniLog::OmniLog()
{
}


OmniLog::~OmniLog()
{
}

#include "OmniHookManager.h"
#include "OmniLog.h"
#include "Action.h"
#include <Windows.h>

OmniHookManager* OmniHookManager::_instance;

ControllerVTableInfo::ControllerVTableInfo(vr::ITrackedDeviceServerDriver* controller)
{
}

ControllerVTableInfo::~ControllerVTableInfo()
{
}

typedef vr::DriverPose_t(*GetPoseFunc)();

DeviceVTableInfo::DeviceVTableInfo(vr::ITrackedDeviceServerDriver* deviceDriver)
{
	//InstallHook(&ActivateHook, OmniHookManager::Activate, deviceDriver, 0, "Activate");
	//InstallHook(&DeactivateHook, OmniHookManager::Deactivate, deviceDriver, 1, "Deactivate");
	//InstallHook(&EnterStandbyHook, OmniHookManager::EnterStandby, deviceDriver, 2, "EnterStandby");
	//InstallHook(&GetComponentHook, OmniHookManager::GetComponent, deviceDriver, 3, "GetComponent");
	//InstallHook(&DebugRequestHook, OmniHookManager::DebugRequest, deviceDriver, 4, "DebugRequest");

	//InstallHook(&GetPoseHook, OmniHookManager::GetPose, deviceDriver, 5, "GetPose");

	//vTable = GetVtable(deviceDriver);
	//auto pose = static_cast<GetPoseFunc>(vTable[4])();
	//OmniLog::AppendLine("OMNI device(%s) updated pose with rotation(%f,%f,%f,%f)", "FUCK", pose.qRotation.w, pose.qRotation.x, pose.qRotation.y, pose.qRotation.z);
}

DeviceVTableInfo::~DeviceVTableInfo()
{
	RemoveHook(&ActivateHook, "Activate");
	RemoveHook(&DeactivateHook, "Deactivate");
	RemoveHook(&EnterStandbyHook, "EnterStandby");
	RemoveHook(&GetComponentHook, "GetComponent");
	RemoveHook(&DebugRequestHook, "DebugRequest");
	RemoveHook(&GetPoseHook, "GetPose");
}

void DeviceVTableInfo::AddDevice(vr::ITrackedDeviceServerDriver* deviceDriver, const char* deviceSerial, vr::ETrackedDeviceClass deviceClass)
{
	OmniLog::AppendLine("Device Added to VTableInfo driver(%p), serial(%s),  deviceClass(%i)", deviceDriver, deviceSerial, deviceClass);
	_devices.emplace_back(new DeviceInfo(deviceDriver, deviceSerial, deviceClass));
}

void DeviceVTableInfo::RemoveDevice(vr::ITrackedDeviceServerDriver* deviceDriver)
{
	std::unique_ptr<DeviceInfo> deviceToRemove = nullptr;
	for(auto iter = _devices.rbegin(); iter != _devices.rend(); ++iter)
	{
		if((*iter)->driver == deviceDriver)
		{
			deviceToRemove = std::move(*iter);
			break;
		}
	}
	if(deviceToRemove != nullptr)
	{
		_devices.remove(deviceToRemove);
	}
}

void DeviceVTableInfo::UpdateNextIndex(uint32_t deviceIndex)
{
	for(auto& currDevice : _devices)
	{
		if(currDevice->index == -1)
		{
			currDevice->index = deviceIndex;
			break;
		}
	}
}

static std::unique_ptr<DeviceInfo> device_not_found;
static subhook::Hook hook_not_found;

std::unique_ptr<DeviceInfo>& DeviceVTableInfo::GetDevice(uint32_t deviceIndex)
{
	for(auto& currDevice : _devices)
	{
		if(currDevice->index == deviceIndex)
		{
			return currDevice;
		}
	}
	return device_not_found;
}

OmniHookManager::OmniHookManager(vr::IVRServerDriverHost* server, std::unique_ptr<OmniHookActions>& actions) : _actions(std::move(actions)), _server(server)
{
	_instance = this;
	//InstallHook(&TrackedDeviceAddedHook, OmniHookManager::TrackedDeviceAdded, server, 0, "TrackedDeviceAdded");
	//InstallHook(&VsyncEventHook, OmniHookManager::VsyncEvent, server, 2, "VsyncEvent");
	//InstallHook(&VendorSpecificEventHook, OmniHookManager::VendorSpecificEvent, server, 3, "VendorSpecificEvent");
	//IsExiting crashes steamVR
	//InstallHook(&IsExitingHook, OmniHookManager::IsExiting, server, 4, "IsExiting");
	//InstallHook(&PollNextEventHook, OmniHookManager::PollNextEvent, server, 5, "PollNextEvent");
	//InstallHook(&GetRawTrackedDevicePosesHook, OmniHookManager::GetRawTrackedDevicePoses, server, 6, "GetRawTrackedDevicePoses");
	//InstallHook(&TrackedDeviceDisplayTransformUpdatedHook, OmniHookManager::TrackedDeviceDisplayTransformUpdated, server, 7, "TrackedDeviceDisplayTransformUpdated");
}

std::unique_ptr<DeviceInfo>& OmniHookManager::GetDevice(uint32_t deviceIndex)
{
	for(auto& currDriver : _driverMap)
	{
		auto& device = currDriver.second->GetDevice(deviceIndex);
		if(device != device_not_found)
		{
			return device;
		}
	}
	return device_not_found;
}

subhook::Hook* OmniHookManager::GetPoseHook(vr::ITrackedDeviceServerDriver* deviceDriver)
{
	auto vtable = GetVtable(deviceDriver);
	auto& driver = _driverMap[vtable];
	return &driver->GetPoseHook;

	//for(auto& currDriver : _driverMap)
	//{
	//	auto& device = currD//currDriver.second->GetDevice(deviceIndex);
	//	if(device != device_not_found)
	//	{
	//		return &currDriver.second->GetPoseHook;
	//	}
	//}
	//return nullptr;
}

OmniHookManager::~OmniHookManager()
{
	RemoveHook(&TrackedDeviceAddedHook, "TrackedDeviceAdded");
	RemoveHook(&TrackedDevicePoseUpdatedHook, "TrackedDevicePoseUpdated");
	RemoveHook(&VsyncEventHook, "VsyncEvent");
	RemoveHook(&VendorSpecificEventHook, "VendorSpecificEvent");
	RemoveHook(&IsExitingHook, "IsExiting");
	RemoveHook(&PollNextEventHook, "PollNextEvent");
	RemoveHook(&GetRawTrackedDevicePosesHook, "GetRawTrackedDevicePoses");
	RemoveHook(&TrackedDeviceDisplayTransformUpdatedHook, "TrackedDeviceDisplayTransformUpdated");
}

bool OmniHookManager::TrackedDeviceAdded(vr::IVRServerDriverHost* server, const char* deviceSerial, vr::ETrackedDeviceClass deviceClass, vr::ITrackedDeviceServerDriver* deviceDriver)
{
	auto deviceVTable = GetVtable(deviceDriver);
	OmniLog::AppendLine("TrackedDeviceAdded for serial(%s) on driver(%p) with Vtable(%p)", deviceSerial, deviceDriver, deviceVTable);

	auto& vTableInfo = _instance->_driverMap[deviceVTable];
	if(vTableInfo == nullptr)
	{
		_instance->_driverMap[deviceVTable].reset(new DeviceVTableInfo(deviceDriver));
		OmniLog::AppendLine("Added new vTableInfo(%p)", vTableInfo.get());
	}

	vTableInfo->AddDevice(deviceDriver, deviceSerial, deviceClass);

	_instance->_actions->TrackedDeviceAdded(server, deviceSerial, deviceClass, deviceDriver);
	subhook::ScopedHookRemove remove(&_instance->TrackedDeviceAddedHook);
	return server->TrackedDeviceAdded(deviceSerial, deviceClass, deviceDriver);
}

vr::EVRInitError OmniHookManager::Activate(vr::ITrackedDeviceServerDriver* deviceDriver, uint32_t deviceIndex)
{

	auto deviceVTable = GetVtable(deviceDriver);
	const auto& deviceVTableInfo = _instance->_driverMap[deviceVTable];

	OmniLog::AppendLine("Hook DeviceActivate deviceVTable(%p) deviceVTableInfo(%p)", deviceVTable, deviceVTableInfo.get());
	if(deviceVTableInfo != nullptr)
	{
		deviceVTableInfo->UpdateNextIndex(deviceIndex);

		std::string serial = "UKNOWN";
		auto& device = _instance->GetDevice(deviceIndex);
		if(device != device_not_found)
		{
			serial = device->serialNumber;
		}

		_instance->_actions->Activate(deviceDriver, serial, deviceIndex, deviceVTableInfo.get());

		subhook::ScopedHookRemove activeRemove(&deviceVTableInfo->ActivateHook);
		return deviceDriver->Activate(deviceIndex);
	}
	return vr::EVRInitError::VRInitError_Driver_Failed;
}

vr::DriverPose_t OmniHookManager::GetPose()//vr::ITrackedDeviceServerDriver* deviceDriver)
{

	//auto deviceVTable = GetVtable(deviceDriver);
	//const auto& deviceVTableInfo = _instance->_driverMap[deviceVTable];

	//vr::DriverPose_t pose;
	//ZeroMemory(&pose, sizeof(pose));
	//pose.poseIsValid = true;

	//OmniLog::AppendLine("GetPose !! deviceVTable(%p) deviceVTableInfo(%p) deviceDriver(%p)", deviceVTable, deviceVTableInfo.get(), deviceDriver);

	//if(deviceVTableInfo != nullptr)
	//{
	//	_instance->_actions->GetPose(deviceDriver, &deviceVTableInfo->GetPoseHook, &pose);
	//}

	return vr::DriverPose_t();
}


void OmniHookManager::TrackedDevicePoseUpdated(vr::IVRServerDriverHost* server, uint32_t deviceIndex, const vr::DriverPose_t & constPose, uint32_t poseSize)
{
	OmniLog::AppendLine("GetPose !! server(%p)", server);
	//std::string serial = "UKNOWN";
	//auto& device = _instance->GetDevice(deviceIndex);
	//if(device != device_not_found)
	//{
	//	serial = device->serialNumber;
	//}

	vr::DriverPose_t pose = constPose;
	_instance->_actions->TrackedDevicePoseUpdated(server, deviceIndex, &pose, poseSize);

	if(deviceIndex == 1)
	{
		subhook::ScopedHookRemove remove(&_instance->TrackedDevicePoseUpdatedHook);
		//_instance->TrackedDevicePoseUpdatedHook.Remove();
		server->TrackedDevicePoseUpdated(deviceIndex, pose, poseSize);
	}
}

void OmniHookManager::VsyncEvent(vr::IVRServerDriverHost* server, double vsyncTimeOffsetSeconds)
{
	_instance->_actions->VsyncEvent(server, vsyncTimeOffsetSeconds);
	subhook::ScopedHookRemove remove(&_instance->VsyncEventHook);
	server->VsyncEvent(vsyncTimeOffsetSeconds);
}


void OmniHookManager::VendorSpecificEvent(vr::IVRServerDriverHost* server, uint32_t deviceIndex, vr::EVREventType eventType, const vr::VREvent_Data_t & eventData, double eventTimeOffset)
{
	std::string serial = "UKNOWN";
	auto& device = _instance->GetDevice(deviceIndex);
	if(device != device_not_found)
	{
		serial = device->serialNumber;
	}
	_instance->_actions->VendorSpecificEvent(server, serial, deviceIndex, eventType, eventData, eventTimeOffset);
	subhook::ScopedHookRemove remove(&_instance->VendorSpecificEventHook);
	server->VendorSpecificEvent(deviceIndex, eventType, eventData, eventTimeOffset);
}

bool OmniHookManager::IsExiting(vr::IVRServerDriverHost* server)
{
	_instance->_actions->IsExiting(server);
	subhook::ScopedHookRemove remove(&_instance->IsExitingHook);
	return server->IsExiting();
}

bool OmniHookManager::PollNextEvent(vr::IVRServerDriverHost* server, vr::VREvent_t * event, uint32_t uncbVREvent)
{
	_instance->_actions->PollNextEvent(server, event, uncbVREvent);
	subhook::ScopedHookRemove remove(&_instance->PollNextEventHook);
	return server->PollNextEvent(event, uncbVREvent);
}

void OmniHookManager::GetRawTrackedDevicePoses(vr::IVRServerDriverHost* server, float secondsFromNow, vr::TrackedDevicePose_t * devicePoseArray, uint32_t devicePoseArrayCount)
{
	_instance->_actions->GetRawTrackedDevicePoses(server, secondsFromNow, devicePoseArray, devicePoseArrayCount);
	subhook::ScopedHookRemove remove(&_instance->GetRawTrackedDevicePosesHook);
	server->GetRawTrackedDevicePoses(secondsFromNow, devicePoseArray, devicePoseArrayCount);
}

void OmniHookManager::TrackedDeviceDisplayTransformUpdated(vr::IVRServerDriverHost* server, uint32_t deviceIndex, vr::HmdMatrix34_t eyeToHeadLeft, vr::HmdMatrix34_t eyeToHeadRight)
{
	std::string serial = "UKNOWN";
	auto& device = _instance->GetDevice(deviceIndex);
	if(device != device_not_found)
	{
		serial = device->serialNumber;
	}
	_instance->_actions->TrackedDeviceDisplayTransformUpdated(server, serial, deviceIndex, eyeToHeadLeft, eyeToHeadRight);
	subhook::ScopedHookRemove remove(&_instance->TrackedDeviceDisplayTransformUpdatedHook);
	server->TrackedDeviceDisplayTransformUpdated(deviceIndex, eyeToHeadLeft, eyeToHeadRight);
}

void OmniHookManager::Deactivate(vr::ITrackedDeviceServerDriver* deviceDriver)
{
	_instance->_actions->Deactivate(deviceDriver);

	auto deviceVTable = GetVtable(deviceDriver);
	const auto& deviceVTableInfo = _instance->_driverMap[deviceVTable];

	deviceVTableInfo->RemoveDevice(deviceDriver);

	subhook::ScopedHookRemove remove(&deviceVTableInfo->DeactivateHook);
	deviceDriver->Deactivate();
}

void OmniHookManager::EnterStandby(vr::ITrackedDeviceServerDriver* deviceDriver)
{
	_instance->_actions->EnterStandby(deviceDriver);

	auto deviceVTable = GetVtable(deviceDriver);
	const auto& deviceVTableInfo = _instance->_driverMap[deviceVTable];

	subhook::ScopedHookRemove remove(&deviceVTableInfo->EnterStandbyHook);
	deviceDriver->EnterStandby();
}

void* OmniHookManager::GetComponent(vr::ITrackedDeviceServerDriver* deviceDriver, const char* pchComponentNameAndVersion)
{
	_instance->_actions->GetComponent(deviceDriver, pchComponentNameAndVersion);

	auto deviceVTable = GetVtable(deviceDriver);
	const auto& deviceVTableInfo = _instance->_driverMap[deviceVTable];

	subhook::ScopedHookRemove remove(&deviceVTableInfo->GetComponentHook);
	return deviceDriver->GetComponent(pchComponentNameAndVersion);
}

void OmniHookManager::DebugRequest(vr::ITrackedDeviceServerDriver* deviceDriver, const char* pchRequest, char* pchResponseBuffer, uint32_t unResponseBufferSize)
{
	_instance->_actions->DebugRequest(deviceDriver, pchRequest, pchResponseBuffer, unResponseBufferSize);

	auto deviceVTable = GetVtable(deviceDriver);
	const auto& deviceVTableInfo = _instance->_driverMap[deviceVTable];

	subhook::ScopedHookRemove remove(&deviceVTableInfo->DebugRequestHook);
	deviceDriver->DebugRequest(pchRequest, pchResponseBuffer, unResponseBufferSize);
}

OmniHookManager* OmniHookManager::GetInstance()
{
	return _instance;
}

void OmniHookManager::Cleanup()
{
	delete _instance;
}
